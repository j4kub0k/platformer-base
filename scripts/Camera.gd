extends Camera2D


# Called when the node enters the scene tree for the first time.
@export var player:CharacterBody2D


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if player.velocity.x > 1:
		drag_horizontal_offset=3
	elif player.velocity.x<0:
		drag_horizontal_offset=-3
	else:
		drag_horizontal_offset=0
	if player.velocity.y>0:
		drag_vertical_offset=3
	elif player.velocity.y<0:
		drag_vertical_offset=-3
	else:
		drag_vertical_offset=0
