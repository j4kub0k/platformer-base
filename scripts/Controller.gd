# Simple movement and jump
# Wrapping around the edges of the screen

extends CharacterBody2D

@export var speed: int = 800
@export var jump_force: int = 750 
@export var acceleratopm: int = 1200
@export var allowed_jump_delay := 0.1
@export var coyote_jump:=0.1

@export var damping :=0.5
@export var accel_damping := 0.5
@export var deccel_damping := 0.02

@export var teleport:Teleport

var on_rope=false
var jumping:bool
var grounded=false
var pressed_jump_time=-1000
var time_on_ground
var RopeEnd=preload("res://scenes/Ropeendpiece.tscn")
var Rope=preload("res://scenes/Ropepiece.tscn")
var rope_holder
var rope_pos
var Fall_playing:bool=false
var flipped:bool=false

# Set the gravity from project settings to be synced with RigidBody nodes
var gravity = ProjectSettings.get_setting("physics/2d/default_gravity") * 2

var velocity_y=0

func _process(delta):
	if on_rope:
		position=rope_pos.position-Vector2(25,0)
		velocity.y=0

func _physics_process(delta):
	velocity_y=velocity.y
	$RayCast2D.target_position=get_local_mouse_position()
	if not is_on_floor():
		velocity.y += gravity * delta
	if not on_rope:
		if Input.is_action_pressed("Left"):
			move_left(delta)
		elif Input.is_action_pressed("Right"):
			move_right(delta)
		else:
			move_stop(delta)
			
		if Input.is_action_just_pressed("Jump"):
			pressed_jump_time=Time.get_ticks_msec()
			jump()
		if  Input.is_action_just_released("Jump") && jumping:
			velocity.y /= 2
	if  velocity.y >0 and  not Fall_playing:
		$AnimationPlayer.play("Fall")
		Fall_playing=true
	if Input.is_action_just_pressed("CastRope") and not on_rope:
		tryrope()
	elif Input.is_action_just_pressed("CastRope") and on_rope:
		rope_holder.queue_free()
		on_rope=false
		$CollisionShape2D.disabled=false
	if Input.is_action_just_pressed("RopeUP") and on_rope:
		RopeUp()
	if Input.is_action_just_pressed("RopeDown") and on_rope:
		RopeDown()
	if Input.is_action_pressed("Teleport"):
		Teleport()
	move_and_slide()
	#ScreenWrap.wrap_x_cbody(self)
		

func Teleport():
	var x=get_global_mouse_position()
	if not teleport.colliding and teleport.in_distance:
		modulate=Color(Color.BLUE,0)
		if(on_rope):
			on_rope=false
			rope_holder.queue_free()
			$CollisionShape2D.disabled=false
		velocity.y=0
		position=x
		Tweens.tween(self,'modulate',modulate,Color(Color.WHITE,1),0.2,EaseType.EASE_IN_CUBIC)
func RopeUp():
	var x = rope_holder.get_child_count()	
	if(x>2):
		var y =rope_holder.get_child(x-1)
		y.queue_free()
		rope_pos=rope_holder.get_child(x-2)
	
func RopeDown():
	if(not grounded):
		var x = rope_holder.get_child_count()
		var y =rope_holder.get_child(x-1)
		var rope = Rope.instantiate()
		rope.position=y.position-Vector2(0,-25)
		rope_holder.add_child(rope)
		y.get_node("PinJoint2D").node_b = rope.get_path()
		rope_pos=rope


func tryrope():
	
	#$RayCast2D.target_position=get_local_mouse_position()
	if !$RayCast2D.is_colliding():
		return
	var ropes_node = get_tree().current_scene.get_node('Ropes')
	rope_holder = Node2D.new()
	ropes_node.add_child(rope_holder)
	var point = $RayCast2D.get_collision_point()
	if point.y<position.y:
		var direction = (point - position).normalized()
		var angle = atan2(direction.y, direction.x)
		var EndRope=RopeEnd.instantiate()
		rope_holder.add_child(EndRope)
		EndRope.position=point
	
		var las_rope=EndRope
		var amount = ceil(position.distance_to(point) / 25)
		for i in range(amount):
			var rope = Rope.instantiate()
			if rope_holder.get_child_count()>1:
				var new_position = las_rope.position - Vector2(cos(angle), sin(angle))*25
				rope.position = new_position
				var relative_angle = atan2(new_position.y - position.y, new_position.x - position.x)
				rope.rotation = relative_angle+ PI/2
			else:
				rope.position=point
				rope.get_node("CollisionShape2D").disabled=true
			rope_holder.add_child(rope)
			las_rope.get_node("PinJoint2D").node_b = rope.get_path()
			las_rope=rope
		#$CollisionShape2D.disabled=true
		rope_pos=las_rope
		on_rope=true
	
	
func move_left(delta):
	if velocity.x > -speed:
		if velocity.x < 0:
			velocity.x -=acceleratopm*delta
			velocity.x *= pow(accel_damping,delta)
		else:
			velocity.x *= pow(accel_damping,delta)
			velocity.x -= acceleratopm * delta
	if not jumping:
		$AnimationPlayer.play("Walk")
		Fall_playing=false
	if not flipped:
		Tweens.tween($Skeleton2D,'scale',$Skeleton2D.scale,Vector2(-1,1),0.2,Ease.EASE_OUT_QUAD)
		flipped=true
	#$Skeleton2D.scale.x=-1   # face left
	
func move_right(delta):
	if velocity.x < speed:
		if velocity.x > 0:
			velocity.x +=acceleratopm*delta
			velocity.x *= pow(accel_damping,delta)
		else:
			velocity.x *= pow(accel_damping, delta)
			velocity.x += acceleratopm * delta
	if not jumping:
		$AnimationPlayer.play("Walk")
		Fall_playing=false
	if flipped:
		Tweens.tween($Skeleton2D,'scale',$Skeleton2D.scale,Vector2(1,1),0.5,EaseType.EASE_OUT_QUAD)
		flipped=false
	#$Skeleton2D.scale.x=1  # face right
	
func move_stop(delta):
	velocity.x *= pow(deccel_damping,delta)
	if not jumping and velocity.y == 0:
		$AnimationPlayer.play("Idle")
		Fall_playing=false
	
func jump():
	if grounded or Time.get_ticks_msec()-time_on_ground<1000*coyote_jump:
		velocity.y = -jump_force
		jumping = true
		
	else:
		pressed_jump_time=Time.get_ticks_msec()
	
func _on_static_body_2d_body_entered(body):
	Fall_playing=false
	grounded = true
	if Time.get_ticks_msec() - pressed_jump_time <1000* allowed_jump_delay:
		velocity.y= -jump_force
		jumping= true
		pressed_jump_time= -1000
		
	else:
		jumping = false
	if velocity_y>1000:
		get_tree().reload_current_scene()
	

func _on_static_body_2d_body_exited(body):
	grounded=false
	time_on_ground=Time.get_ticks_msec()
	if not on_rope:
		$AnimationPlayer.play("Jump")
		Fall_playing=false
