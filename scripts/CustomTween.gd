extends Node


class_name CustomTween


var t = 0
var real
var target
var property
var start
var end
var dur
var easing

func _init(target, property ,start, end, duration, ease_type):
	self.target = target
	self.property = property
	self.start = start
	self.end = end
	self.dur = duration
	self.easing= ease_type
func update(delta: float):
	t += delta / dur
	match easing:
		EaseType.LINEAR_EASE:
			real= linear_ease()
		EaseType.EASE_IN_CUBIC:
			real=ease_in_cubic()
		EaseType.EASE_IN_OUT_CUBIC:
			real=ease_in_out_cubic()
		EaseType.EASE_IN_OUT_QUAD:
			real=ease_in_out_quad()
		EaseType.EASE_IN_OUT_SINE:
			real=ease_in_out_sine()
		EaseType.EASE_IN_QUAD:
			real=ease_in_quad()
		EaseType.EASE_IN_SINE:
			real=ease_in_sine()
		EaseType.EASE_OUT_CUBIC:
			real=ease_out_cubic()
		EaseType.EASE_OUT_QUAD:
			real=ease_out_quad()
		EaseType.EASE_OUT_SINE:
			real=ease_out_sine()
			
	real*= (end - start) + start
	target.set(property, real)


func is_done() -> bool:
	return t > 1.0
	
func linear_ease():
	return t

func ease_in_quad():
	return t*t
	
func ease_out_quad():
	return 1 - (1-t)*(1-t)

func ease_in_out_quad():
	if(t < 0.5):
		return 2 * ease_in_quad()
	return 2 * ease_out_quad();

func ease_in_cubic():
	return t*t*t
	
func ease_out_cubic():
	return 1 - (1-t)*(1-t)*(1-t)
	
func ease_in_out_cubic():
	if(t < 0.5):
		return 4 * ease_in_cubic()
	return 4 * ease_out_cubic()

func smoothstep3():
	return lerp(ease_in_quad(),ease_out_quad(),t)
	
func ease_in_sine():
	return 1 - cos(t * PI/2)
	
func ease_out_sine():
	return sin(t * PI/2)

func ease_in_out_sine():
	return -(cos(t * PI) - 1) / 2
