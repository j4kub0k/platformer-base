extends Area2D

class_name Teleport

var colliding:bool=false
var in_distance=false
@export var player:CharacterBody2D


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$CollisionShape2D.position=get_local_mouse_position()
	$Sprite2D.position=get_local_mouse_position()
	if colliding or position.distance_to($Sprite2D.position)>=400:
		in_distance=false
		$Sprite2D.modulate=Color(Color.RED,0.4)
	else:
		in_distance=true
		$Sprite2D.modulate=Color(Color.WHITE,0.4)

func _on_body_entered(body):
	colliding=true


func _on_body_exited(body):
	colliding=false
